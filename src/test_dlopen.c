/*
 * Copyright (C) 2016  Éric Dorino.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <sys/stat.h>
#include <dlfcn.h>
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

int main(int argc, char **argv)
{
#ifdef PAM_STATIC
	return 50;
#else
	int i;
	struct stat st;
	char buf[PATH_MAX];

	if (argc < 2) {
		fputs("usage: test_dlopen <module>...\n", stderr);
		return 1;
	}

	for (i = 1; i < argc; i++) {
		if (dlopen(argv[i], RTLD_NOW)) {
			printf("dlopen() of \"%s\" succeeded.\n", argv[i]);
		}
		else {
			snprintf(buf, sizeof(buf), "./%s", argv[i]);
			if ((stat(buf, &st) == 0) && dlopen(buf, RTLD_NOW)) {
				printf("dlopen() of \"./%s\" succeded.\n", argv[i]);
			}
			else {
				printf("dlopen() of \"%s\" failed: %s\n", argv[i], dlerror());
				return 1;
			}
		}
	}
	return 0;
#endif
}
