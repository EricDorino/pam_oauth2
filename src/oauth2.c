/*
 * Copyright (C) 2018  Éric Dorino.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <oauth2d.h>
#include <pwd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>
#include <time.h>

#define PAM_SM_AUTH
#define PAM_SM_ACCOUNT

#include <security/pam_appl.h>
#include <security/pam_ext.h>
#include <security/pam_modules.h>

static int get_token(pam_handle_t *pamh)
{
	int ret;
	char *token;

	ret = pam_prompt(pamh, PAM_PROMPT_ECHO_ON, &token, "Token: ");
	if (ret != PAM_SUCCESS)
		return ret;
	if (!token)
		return PAM_CONV_ERR;
	return pam_set_item(pamh, PAM_AUTHTOK, token);
}

static int auth(pam_handle_t *pamh, int flags, int argc, const char **argv)
{
	int ret;
	const char *user, *token;
	struct oa2_token t;
	struct oa2_client c;
	struct passwd passwd;

	if ((ret = pam_get_user(pamh, &user, NULL)) != PAM_SUCCESS) {
		pam_syslog(pamh, LOG_DEBUG, "pam_get_user(): %s", pam_strerror(pamh, ret));
		return ret;
	}
	if ((ret = get_token(pamh)) != PAM_SUCCESS) {
		pam_syslog(pamh, LOG_DEBUG, "get_token(): %s", pam_strerror(pamh, ret));
		return ret;
	}
	if ((ret = pam_get_item(pamh, PAM_AUTHTOK, (const void **)&token)) !=
									PAM_SUCCESS) {
		pam_syslog(pamh, LOG_DEBUG, "pam_get_item(AUTHTOK): %s", 
										pam_strerror(pamh, ret));
		return ret;
	}
	if (!oa2_token_find(token, &t)) {
		pam_syslog(pamh, LOG_CRIT, "Invalid token");
		return PAM_AUTH_ERR;
	}
	if (t.refresh) {
		pam_syslog(pamh, LOG_CRIT, "Invalid token");
		return PAM_AUTH_ERR;
	}
	if (!t.user_id || (t.user_id && strcmp(t.user_id, user) != 0)) {
		pam_syslog(pamh, LOG_CRIT, "User %s unknown", user);
		return PAM_USER_UNKNOWN;
	}
	if (!getpwnam(t.user_id)) {
		pam_syslog(pamh, LOG_CRIT, "User %s unknown", t.user_id);
		return PAM_USER_UNKNOWN;
	}
	if (t.validity < 0 || (t.validity > 0 && t.validity < time(NULL))) {
		pam_syslog(pamh, LOG_CRIT, "Expired token");
		return PAM_AUTH_ERR;
	}
	if (!oa2_client_find(t.client_id, &c)) {
		pam_syslog(pamh, LOG_CRIT, "Unknown client");
		return PAM_AUTH_ERR;
	}
	return PAM_SUCCESS;
}

PAM_EXTERN int pam_sm_authenticate(pam_handle_t *pamh,
								int flags, 
								int argc, const char **argv)
{
	return auth(pamh, flags, argc, argv);
}

PAM_EXTERN int pam_sm_setcred(pam_handle_t *pamh,
								int flags, 
								int argc, const char **argv)
{
	return PAM_SUCCESS;
}

PAM_EXTERN int pam_sm_acct_mgmt(pam_handle_t *pamh,
								int flags, 
								int argc, const char **argv)
{
	return auth(pamh, flags, argc, argv);
}

PAM_EXTERN int pam_sm_open_session(pam_handle_t *pamh,
								int flags, 
								int argc, const char **argv)
{
	return PAM_IGNORE;
}

PAM_EXTERN int pam_sm_close_session(pam_handle_t *pamh,
								int flags, 
								int argc, const char **argv)
{
	return PAM_IGNORE;
}

PAM_EXTERN int pam_sm_chauthtok(pam_handle_t *pamh,
								int flags, 
								int argc, const char **argv)
{
	return PAM_IGNORE;
}

#ifdef PAM_STATIC
struct pam_module _pam_oauth2_modstruct = {
	"pam_oauth2",
	pam_sm_authenticate,
	pam_sm_setcred,
	pam_sm_acct_mgmt,
	pam_sm_open_session,
	pam_sm_close_session,
	pam_sm_chauthtok
};
#endif
